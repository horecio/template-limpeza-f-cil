<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Limpeza Fácil - Loja</title>
	
	<link href="css/waves.css" rel="stylesheet">
	<script type="text/javascript" src="js/waves.js"></script>
	
	<link href="css/sprites.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.livequery.js"></script>
</head>
	<body>
	<!--  top ** -->
		 <?php require_once 'nav.php';?>
	<div class="container content">
		 <!-- produtos-->
		 <?php require_once 'produtos.html';?>
		 <!-- cart-->
		 <?php require_once 'cart.php';?>
	</div>
	<!-- *** Footer *** -->
		<?php require_once 'footer.html';?>
    <!-- *** SCRIPT CART *** -->
    
<script type="text/javascript">
    
    $(document).ready(function() {
    	if($('.cart-total span').html() != "0,00"){
    		$('#empty').html("");
		} 
    	var inputId = $('#arrays').attr('value'); 
    	inputId = inputId.split(",");
        var Arrays = inputId;

    	$('.produto button, #md-footer button').click(function(){

        $("#description").attr('style', '');
        var element = $(this).parent();
    	var thisID = element.attr('id');
    	var itemname  = element.find('div .name-prod').html();
    	var itemprice = element.find('div .price').html();
    	
    	itemprice = removeVirgula(itemprice);
    	
    	$('#message-cart-warning').attr('style', 'display:none');
        
    	if(include(Arrays, thisID))
    	{
    		var price = $('#each-'+thisID).children(".shopp-price").find('#price').html();
    		//Remove ponto e virgula
    		price = removeVirgula(price);
    		
    		var quantity = $('#each-'+thisID).children(".input-hidden-quantity").attr('value');
    		quantity = parseInt(quantity) + parseInt(1);
    		
    		var total = itemprice * quantity;
    		total = formatReal(total);
    		
    		$('#each-'+thisID).children(".shopp-price").find('#price').html(total);
    		$('#each-'+thisID).children(".shopp-price").find('#price-hidden').attr('value', total);
    		$('#each-'+thisID).children(".shopp-quantity").html("Qtde: <span>" + quantity + "</span>");
    		$('#each-'+thisID).children(".input-hidden-quantity").attr('value', quantity);
    		
    		var prev_charges = $('.cart-total span').html();
    		prev_charges = removeVirgula(prev_charges);
    		total = removeVirgula(total);
    	
    		prev_charges = prev_charges - price;
    		prev_charges = prev_charges + total;
    		
    		// Transforma em formato moeda
    		prev_charges = formatReal(prev_charges);
    		
    		$('.cart-total span').html(prev_charges);
    		
    		$('#total-hidden-charges').val(prev_charges);
    	}else{
    		Arrays.push(thisID);
    		$('#arrays').attr('value', Arrays);
    		
    		var prev_charges = $('.cart-total span').html();
    		prev_charges = removeVirgula(prev_charges);
    		
    		prev_charges = prev_charges + itemprice;
    		
    		var total = prev_charges;
    		// Transforma em formato moeda 
    		total = formatReal(total);
    		itemprice = formatReal(itemprice);
    		
    		$('.cart-total span').html(total);
    		$('#total-hidden-charges').val(total);
    		
    		if($('#description span').html() == "Seu Carrinho está vazio."){
    			$('#description span').html('');
    		}
    		$('#cart .cart-info').append('<div class="shopp borda-redonda" id="each-'+thisID+'"><input type="hidden" name="'+thisID+'" value="'+thisID+'" /><div class="nome-produto">'+itemname+'</div><input type="hidden" name="name'+thisID+'" value="'+itemname+'" /><div class="shopp-price prod-descr"> R$ <span id="price">'+itemprice+'</span><input type="hidden" id="price-hidden" name="price'+thisID+'" value="'+itemprice+'" /></div><div class="shopp-quantity prod-descr">Qtde: <span>1</span></div><input class="input-hidden-quantity" type="hidden" name="qtde'+thisID+'" value="1" /><span id="remove" class="fa-trash remove"></span></div>');
    	}
    	});	
    	
    	$('.remove').livequery('click', function() {
    		var deduct = $(this).parent().children(".shopp-price").find('#price').html();
    		deduct = removeVirgula(deduct);
    		
    		var prev_charges = $('.cart-total span').html();
    		prev_charges = removeVirgula(prev_charges);		
    		
    		var thisID = $(this).parent().attr('id').replace('each-','');
    		
    		var pos = getpos(Arrays,thisID);
    		Arrays.splice(pos,1,"0,00");
    		
    		prev_charges = (prev_charges - deduct);
    		prev_charges = formatReal(prev_charges);
    		
    		if(prev_charges == 0) {
    			prev_charges = "0,00";
    			$('#description span').html("Seu Carrinho está vazio.");
    			$("#description").attr('style', 'background-color: white;text-align: center;');
    		}
    		$('.cart-total span').html(prev_charges);
    		$('#total-hidden-charges').val(prev_charges);
    		$(this).parent().remove();
    	});	
    	
    	$('#cart-submit').livequery('click', function() {
    		if(!($('#description span').html() == "Seu Carrinho está vazio."))
    		{
    			return true;
    		}else{
    			$('#message-cart-warning').attr('style', 'display:block');
    			return false;
    		}
    	});	
    });
    
    function include(arr, obj) {
      for(var i=0; i<arr.length; i++) {
        if (arr[i] == obj) return true;
      }
    }
    function getpos(arr, obj) {
      for(var i=0; i<arr.length; i++) {
        if (arr[i] == obj) return i;
      }
    }
    function removeVirgula(value){
    	string = value.toString();
    	return parseInt( string.replace(/[\D]+/g,'') );
    	//return string.replace(',', '');
    }
    
   /*** SCRIPT FORMAT NUMBER ***/
    function formatReal( int ){
        var tmp = int+'';
        tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
        if( tmp.length > 6 )
        tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
         
        return tmp;
    }
    </script>
	</body>
</html>