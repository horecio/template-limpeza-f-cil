<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Limpeza Fácil</title>
	
	<link href="css/waves.css" rel="stylesheet">
	
	
	<link href="css/sprites.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

	<script type="text/javascript" src="js/waves.js"></script>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.livequery.js"></script>
	
</head>
	<body>
	 <!--  top-menu ** -->
		 <?php require_once 'nav.php';?>
	<div class="container">
		<!-- *** content *** -->
		<?php require_once 'conteudo.html';?>
	</div>
	<!-- *** Footer *** -->
	<?php require_once 'footer.html';?>
	</body>
</html>