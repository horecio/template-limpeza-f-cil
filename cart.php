<div class="container-cart">
<div id="cart" class="borda-redonda">
	<h2 class="text-center">
		<b>Seu carrinho</b>
	</h2>
<form action="assinatura.php" method="post" id="cart_form" name="cart_form">
	<div id="description" class="wrapper" style="background-color: white;text-align: center;">
		<div class="cart-info">
		<?php $product = array_chunk($_POST, 4);
		      $value = end($product);
		    for ($i = 0; $i < count($product) - 1 ; $i++):
		        $id = $product[$i][0];
		        $name = $product[$i][1];
		        $price = $product[$i][2];
		        $quantity = $product[$i][3];
		        ?>
                        <div id="each-<?=$id?>" class="shopp borda-redonda">
                        <input type="hidden" name="<?=$id?>" value="<?=$id?>" />
                        <div class="nome-produto"><?=$name?></div>
                        	<input type="hidden" name="name<?=$id?>" value="<?=$name?>" />
                        	<div class="shopp-price prod-descr text-center">
                        		R$ <span id="price"><?=$price?></span>
                        		<input id="price-hidden" type="hidden" name="price<?=$id?>" value="<?=$price?>" />
                        	</div>
                        	<div class="shopp-quantity prod-descr">Qtde: <span><?=$quantity?></span></div>
                        	<input class="input-hidden-quantity" type="hidden" name="qtde<?=$id?>" value="<?=$quantity?>" />
                        	<span id="remove" class="fa-trash remove">
                        	</span>
                        </div>
	                    <?php endfor;?>
				</div>
                    <span id="empty">Seu Carrinho está vazio.</span>
			</div>
	<div class="cart-total">						
		<b>Total:</b> R$ <span><?php echo $totalPrice = (empty($value[0])) ? '0,00' : $value[0]; ?></span>							
		<input type="hidden" name="total-hidden-charges" id="total-hidden-charges" value="<?=$totalPrice?>" />
		<input id="arrays" type="hidden" value="<?php echo $arrayId = (empty($value[1])) ? '0' : $value[1]; ?>" name="array-id">
	</div>
	<div class="text-center btn-cart">
		<button class="waves-effect waves-button waves-light waves-float btn" type="submit" id="cart-submit" >
		ASSINAR
		</button>	
	</div>
</form>	
<div id="message-cart-warning" class="warning borda-redonda text-center" style="display:none;">
Seu Carrinho Está Vazio
</div>			
</div>

</div>