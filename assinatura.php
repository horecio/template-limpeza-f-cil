<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    	<title>Limpeza Fácil</title>
    	
	<link href="css/waves.css" rel="stylesheet">
	<script type="text/javascript" src="js/waves.js"></script>
	
	<link href="css/sprites.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.livequery.js"></script>
    	
    </head>
	<body>
		<?php require_once 'nav.php';?>
		 
	<div class="container content">
		<?php require_once 'cart.php';?>
		
		<?php require_once 'cadastro.html';?>
	</div>
	   <?php require_once 'footer.html';?>
	   
	   <script type="text/javascript">
	   jQuery(function($) {
			$("#cart_form").attr("action", "loja.php"); 
			$("#empty").html("");
			$(".remove").attr("class", "fa-ok");
			$("#cart-submit").html('Alterar Produtos');
		});
	   </script>
	</body>
</html>